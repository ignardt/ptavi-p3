import smil
import sys


def main (path):
    if path == None:
        print("Usage: python3 elements.py <file>" )
    else:
        objeto = smil.SMIL(path)
        for partes in objeto.elements():
            print(partes.name())


if __name__ == "__main__":
    main(sys.argv[1])
