
import xml.dom.minidom


def main(path):
    document = xml.dom.minidom.parse(path)
    calificaciones = ['buenisimo', 'bueno', 'regular', 'malo', 'malisimo']

    if document.getElementsByTagName('humor') :
        jokes = document.getElementsByTagName('chiste')
        chiste = {}

        for joke in jokes:
            score = joke.getAttribute('calificacion')
            questions = joke.getElementsByTagName('pregunta')
            answers = joke.getElementsByTagName('respuesta')
            question = questions[0].firstChild.nodeValue.strip()
            answer = answers[0].firstChild.nodeValue.strip()
            chiste[score] = [question, answer]

        for cal in calificaciones:
            c = list(filter(lambda n: (n == cal), chiste))
            if len(c) != 0:
                i = c[0]
                if cal == i:
                    print(f"Calificación: {i}.")
                    print(f" Pregunta: {chiste.get(i)[0]}")
                    print(f" Respuesta: {chiste.get(i)[1]}\n")
            else:
                pass

    else:
        raise Exception ("Root element is not humor")


if __name__ == "__main__":
    main("chistes.xml")